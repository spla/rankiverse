# rankiverse
This code gets all public statuses of your Mastodon server local users to calc their `rankiverse` according with their public statuses, followers and how many
boosts and favs they get. Then, it calculates their local and fediverse ratios.


Getting more followers is not important. Getting more reblogs or favs is not important. What really matters is how far the posts are going and how many remote 
servers are interacting with them!
We can say that this code is trying to measure the quality of the fediverse posts.

Calculated ratios are:

- local favourites ratio (local favs divided by local followers)
- federated favourites ratio (favourites from remote servers)
- local reblogs ratio (local reblogs divided by local followers)
- federated reblogs ratio (reblogs from remote servers)

Thanks to these ratios is possible to know the acceptance level of your local users posts and how they are transcending to other servers.
The code also gets how many remote servers are faving or reblogging your local users so how deep they posts are going into the fediverse.

postratios.py is a accepting queries bot. Your Mastodon server local users can ask it for their ratios. Query format is:

@your_bot_username ratios

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon running server (admin).

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.

3. Run `python setup.py` to setup the access tokens for the bot `postratios.py` and its post language (cat or eng only).

4. Run `python rankiverse.py` to get data from your Mastodon's database and write ratios to new database create above.

5. Use your favourite scheduling method to set `python rankiverse.py` to run regularly. 

6. Use your favourite scheduling method to set `python postratios.py` to run every minute.

