#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
start_time = time.time()
from six.moves import urllib
from datetime import datetime
from mastodon import Mastodon
import threading
import os
import requests
import json
import signal
import sys
import os.path
import requests
import operator
import calendar
import psycopg2

from itertools import product

from multiprocessing import Pool, Lock, Process, Queue, current_process
import queue
import multiprocessing

from decimal import *
getcontext().prec = 2

def calc_ratios(userid):

    conn = None

    try:

        conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select username, statuses, local_reblogs, fediverse_reblogs, local_favs, fediverse_favs, local_followers, fediverse_followers, reblogging_servers, faving_servers from spreading where id = (%s)", (userid,))

        row = cur.fetchone()

        if row != None:

            username = row[0]
            statuses = row[1]
            loc_reblog = row[2]
            fed_reblog = row[3]
            loc_fav= row[4]
            fed_fav = row[5]
            loc_foll = row[6]
            fed_foll= row[7]
            reb_servers = row[8]
            fav_servers = row[9]

            cur.close()

        else:

            return

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    if loc_foll > 0:
        lreb_ratio = round((loc_reblog / loc_foll),2)
        lfav_ratio = round((loc_fav / loc_foll),2)
    else:
        lreb_ratio = 0
        lfav_ratio = 0
    if fed_foll > 0:
        freb_ratio = round((fed_reblog / fed_foll),2)
        ffav_ratio = round((fed_fav / fed_foll),2)
    else:
        freb_ratio = 0
        ffav_ratio = 0

    print("-------------------------------------------------------------")
    print("user " + str(username) + ", followers(local/fediverse): " + str(loc_foll) + "/" + str(fed_foll))
    print("statuses: " + str(statuses))
    print("local reblogs ratio: " + str(lreb_ratio) + " / " + "fediverse reblogs ratio: " + str(freb_ratio))
    print("local favs ratio: " + str(lfav_ratio) + " / " + "fediverse favs ratio: " + str(ffav_ratio))
    print("reblogged by servers: " + str(reb_servers))
    print("favourited by servers: " + str(fav_servers))

    conn = None

    try:

        conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        insert_query = "INSERT INTO ratios(username, local_reblogs_ratio, local_favs_ratio, fediverse_reblogs_ratio, fediverse_favs_ratio, reblogging_servers, faving_servers) VALUES(%s,%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        cur.execute(insert_query, (username, lreb_ratio, freb_ratio, lfav_ratio, ffav_ratio, reb_servers, fav_servers))

        cur.execute("UPDATE ratios SET local_reblogs_ratio=(%s), local_favs_ratio=(%s), fediverse_reblogs_ratio=(%s), fediverse_favs_ratio=(%s), reblogging_servers=(%s), faving_servers=(%s) where username = (%s)", (lreb_ratio, freb_ratio, lfav_ratio, ffav_ratio, reb_servers, fav_servers, username))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_toots(userid):


    global user_toots
    global now
    reblogged = 0
    local_reblogs = 0
    fediverse_reblogs = 0
    local_reblogs_total = 0
    fediverse_reblogs_total = 0
    local_favs = 0
    fediverse_favs = 0
    local_favs_total = 0
    fediverse_favs_total = 0
    favourited = 0
    public_toots = []

    local_followers = 0
    fediverse_followers = 0
    reblogs_domains_lst = []
    domains_lst = []

    conn = None
    try:

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select id from statuses where account_id = (%s) and reply='f' and visibility='0' and text != ''", (userid,))

        rows = cur.fetchall()

        if rows != []:

            for row in rows:

                public_toots.append(row[0])

            cur.execute("select count(*) from accounts inner join follows on (accounts.id=follows.account_id) where follows.target_account_id = (%s) and domain is null", (userid,))
            row = cur.fetchone()
            if row != None:
                local_followers = row[0]
            else:
                local_followers = 0

            cur.execute("select count(*) from accounts inner join follows on (accounts.id=follows.account_id) where follows.target_account_id = (%s) and domain is not null", (userid,))
            row = cur.fetchone()
            if row != None:
                fediverse_followers = row[0]
            else:
                fediverse_followers = 0

            cur.close()

        else:

            return

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    user_toots = len(public_toots)

    i = 0
    while i < len(public_toots):

        local_reblogs = 0
        fediverse_reblogs = 0
        local_favs = 0
        fediverse_favs = 0

        conn = None
        try:

            conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            status_id = public_toots[i]

            cur.execute("select reblogs_count, favourites_count from status_stats where status_id = (%s)", (status_id,))
            row = cur.fetchone()

            if row != None:

                reblogged = row[0]
                favourited = row[1]

                if reblogged > 0:

                   cur.execute("select count(account_id) from statuses where reblog_of_id = (%s) and local", (status_id,))
                   row = cur.fetchone()
                   if row != None:
                       local_reblogs = row[0]
                   else:
                       local_reblogs = 0
                   cur.execute("select count(account_id) from statuses where reblog_of_id = (%s) and not local", (status_id,))
                   row = cur.fetchone()
                   if row != None:
                       fediverse_reblogs = row[0]
                   else:
                       fediverse_reblogs = 0
                   cur.execute("select account_id from statuses where reblog_of_id = (%s) and not local", (status_id,))
                   rows = cur.fetchall()
                   for row in rows:
                       reblogs_domains_lst.append(row[0]) if row[0] not in reblogs_domains_lst else reblogs_domains_lst

                if favourited > 0:

                   cur.execute("select account_id from favourites where status_id = (%s)", (status_id,))
                   rows = cur.fetchall()
                   for row in rows:
                       fav_acct_id = row[0]
                       cur.execute("select domain from accounts where id = (%s)", (fav_acct_id,))
                       fav_domain = cur.fetchone()
                       if fav_domain[0] == None:
                           local_favs = local_favs + 1
                       elif fav_domain[0] != None:
                           fediverse_favs = fediverse_favs + 1
                           domains_lst.append(fav_domain[0]) if fav_domain[0] not in domains_lst else domains_lst

                if reblogged or favourited > 0:

                    cur.execute("select username from accounts where id = (%s)", (userid,))
                    row = cur.fetchone()
                    user_username = row[0]

                    reblogged_by_servers = len(reblogs_domains_lst)
                    fav_by_servers = len(domains_lst)

                    print("----------------------------------------------------------------")
                    print("status " + str(i) + " of " + str(len(public_toots)))
                    print("status_id: " + str(status_id))
                    print("by " + str(user_username) + ": reblogs (local/fediverse): " + str(local_reblogs) + "/" + str(fediverse_reblogs))
                    print("by " + str(user_username) + ": favs (local/fediverse): " + str(local_favs) + "/" + str(fediverse_favs))
                    print("reblogged by distinct fediverse servers: " + str(reblogged_by_servers))
                    print("favourited by distinct fediverse servers: " + str(fav_by_servers))
                    print(str(user_username) + "'s followers (local/fediverse): " +str(local_followers) + "/" + str(fediverse_followers))

                    cur.close()

                    insert_query = "INSERT INTO spreading(id, username, statuses, local_reblogs, fediverse_reblogs, local_favs, fediverse_favs, local_followers, fediverse_followers,  faving_servers, reblogging_servers, updated_at) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

                    conn = None

                    try:

                        conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                        cur = conn.cursor()

                        local_reblogs_total = local_reblogs_total + local_reblogs
                        fediverse_reblogs_total = fediverse_reblogs_total + fediverse_reblogs
                        local_favs_total = local_favs_total + local_favs
                        fediverse_favs_total = fediverse_favs_total + fediverse_favs

                        cur.execute(insert_query, (userid, user_username, user_toots, local_reblogs_total, fediverse_reblogs_total, local_favs_total, fediverse_favs_total, local_followers, fediverse_followers, fav_by_servers, reblogged_by_servers, now))

                        cur.execute("UPDATE spreading SET statuses=(%s), local_reblogs=(%s), fediverse_reblogs=(%s), local_favs=(%s), fediverse_favs=(%s), local_followers=(%s), fediverse_followers=(%s), faving_servers = (%s), reblogging_servers = (%s), updated_at=(%s) where username=(%s)", (user_toots, local_reblogs_total, fediverse_reblogs_total, local_favs_total, fediverse_favs_total, local_followers, fediverse_followers, fav_by_servers, reblogged_by_servers, now, user_username))

                        conn.commit()

                    except (Exception, psycopg2.DatabaseError) as error:

                        print(error)

                    finally:

                        if conn is not None:

                            conn.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        i += 1

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load DB configuration from config file
config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
toots_db = get_parameter("toots_db", config_filepath)
toots_db_user = get_parameter("toots_db_user", config_filepath)

###############################################################################
# main

if __name__ == '__main__':

    now = datetime.now()

    users_id = []

    try:

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select accounts.id from accounts, users where (select actor_type = 'Person' or actor_type is null) and users.account_id =  accounts.id")

        rows = cur.fetchall()

        for row in rows:

            users_id.append(row[0])

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ###############################################################################
    # usage modes

    if len(sys.argv) == 1:

        print('usage: python ' + sys.argv[0] + ' --multi' + ' (multiprocessing, fast)')
        print('usage: python ' + sys.argv[0] + ' --mono' + ' (one process, slow)')
        print('usage: python ' + sys.argv[0] + ' --ratio' + ' (multiprocessing, fast)')
        print('usage: python ' + sys.argv[0] + ' --monoratio' + ' (one process, slow)')

    elif len(sys.argv) == 2:

        if sys.argv[1] == '--multi':

            nprocs = multiprocessing.cpu_count()
            with multiprocessing.Pool(processes=32) as pool:
                results = pool.starmap(get_toots, product(users_id))

            nprocs = multiprocessing.cpu_count()
            with multiprocessing.Pool(processes=32) as pool:
                results = pool.starmap(calc_ratios, product(users_id))

        if sys.argv[1] == '--ratio':
            nprocs = multiprocessing.cpu_count()
            with multiprocessing.Pool(processes=32) as pool:
                results = pool.starmap(calc_ratios, product(users_id))

        if sys.argv[1] == '--mono':

            i = 0

            while i < len(users_id):

                get_toots(users_id[i])
                i += 1

        if sys.argv[1] == '--monoratio':

            i = 0

            while i < len(users_id):

                calc_ratios(users_id[i])
                i += 1

    print("\n")
    print("Done!")
    print("Execution time: %s seconds" % round((time.time() - start_time),2))
