#!/usr/bin/env python
# -*- coding: utf-8 -*-

from six.moves import urllib
from datetime import datetime, timedelta
import pytz
from subprocess import call
from mastodon import Mastodon
import unidecode
import time
import threading
import re
import os
import json
import time
import signal
import sys
import os.path        # For checking whether secrets file exists
import requests       # For doing the web stuff, dummy!
import operator       # serveix per poder assignar el valor d'elements de un diccionari a una variable 15/07/18
import calendar
import psycopg2

from decimal import *
getcontext().prec = 2

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def unescape(s):
    s = s.replace("&apos;", "'")
    return s

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    bot_username = get_parameter("bot_username", config_filepath)

    # Load DB configuration from config file
    config_filepath = "config/db_config.txt"
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
    toots_db = get_parameter("toots_db", config_filepath)
    toots_db_user = get_parameter("toots_db_user", config_filepath)

    # Load language configuration from config file
    lang_config_filepath = "config/lang_config.txt"
    bot_lang = get_parameter("bot_lang", lang_config_filepath)

    if bot_lang == "cat":

        language_filepath = "cat.txt"

    elif bot_lang == "eng":

        language_filepath = "eng.txt"

    else:

        print("\nOnly 'cat' and 'eng' languages are supported.\n")
        sys.exit(0)

    toot_text1 = get_parameter("toot_text1", language_filepath)
    toot_text2 = get_parameter("toot_text2", language_filepath)
    toot_text3 = get_parameter("toot_text3", language_filepath)
    toot_text4 = get_parameter("toot_text4", language_filepath)
    toot_text5 = get_parameter("toot_text5", language_filepath)
    toot_text6 = get_parameter("toot_text6", language_filepath)
    toot_text7 = get_parameter("toot_text7", language_filepath)
    toot_text8 = get_parameter("toot_text8", language_filepath)
    toot_text9 = get_parameter("toot_text9", language_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    #############################################################

    html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;",
        }

    #############################################################

    tz = pytz.timezone('Europe/Madrid')
    now = datetime.now(tz)

    queries = 0

    ###################################################################
    # query status_created_at of last notification

    try:

        conn = None

        conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select status_created_at from botreplies order by status_created_at desc limit 1")

        row = cur.fetchone()
        if row != None:
            last_posted = row[0]
            last_posted = last_posted.strftime("%d/%m/%Y, %H:%M:%S")
        else:
            last_posted = ""

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ###################################################################################################################################
    # get bot_id from bot's username

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select id from accounts where username = (%s) and domain is null", (bot_username,))

        row = cur.fetchone()

        if row != None:

            bot_id = row[0]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    #############################################################################################################################
    # check if any new notifications by comparing newest notification datetime with the last query datetime

    last_notifications = [] # to store last 20 'Mention' type notitifications for our bot

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select * from notifications where activity_type = 'Mention' and account_id = (%s) order by created_at desc limit 1", (bot_id,))

        row = cur.fetchone()

        if row != None:

            last_notif_created_at = row[3]

            last_notif_created_at = last_notif_created_at + timedelta(hours=2)

            last_notif_created_at = last_notif_created_at.strftime("%d/%m/%Y, %H:%M:%S")

            if last_posted != "":

                if last_notif_created_at == last_posted:

                    cur.close()

                    conn.close()

                    print("No new notifications")

                    sys.exit(0)

        cur.execute("select * from notifications where activity_type = 'Mention' and account_id = (%s) order by created_at desc limit 20", (bot_id,))

        rows = cur.fetchall()
        if rows != None:

            for row in rows:

               last_notifications.append(row)

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ####################################################################

    i = 0
    while i < len(last_notifications):

        user_id = last_notifications[i][5]
        activity_id = last_notifications[i][0]
        n_created_at = last_notifications[i][3]

        n_created_at = n_created_at + timedelta(hours=2)


        n_created_datetime = n_created_at.strftime("%d/%m/%Y, %H:%M:%S")

        if last_posted != "":

            if n_created_datetime == last_posted:

                print("No more notifications")

                sys.exit(0)

        try:

            conn = None

            conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute("select username, domain from accounts where id=(%s)", (user_id,))

            row = cur.fetchone()

            if row != None:

                username = row[0]
                domain = row[1]

            if domain != None:

                i += 1
                continue

            cur.execute("select status_id from mentions where id = (%s)", (activity_id,))

            row = cur.fetchone()

            if row != None:

                status_id = row[0]

            cur.execute("select text, visibility from statuses where id = (%s)", (status_id,))

            row = cur.fetchone()

            if row != None:

                text = row[0]
                visibility = row[1]

            cur.close()

            if visibility == 0:
                visibility = 'public'
            elif visibility == 1:
                visibility = 'unlisted'
            elif visibility == 2:
                visibility = 'private'
            elif visibility == 3:
                visibility = 'direct'

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        ignore = False

        if domain != None:

            i += 1

        try:

            conn = None
            conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            ### check if already replied

            cur.execute("SELECT id, query_user FROM botreplies where id=(%s)", (status_id,))

            row = cur.fetchone()

            if row == None:

                content = cleanhtml(text)
                content = unescape(content)

                try:

                    start = content.index("@")
                    end = content.index(" ")
                    if len(content) > end:

                        content = content[0: start:] + content[end +1::]

                    neteja = content.count('@')

                    i = 0
                    while i < neteja :

                        start = content.rfind("@")
                        end = len(content)
                        content = content[0: start:] + content[end +1::]
                        i += 1

                    content = content.lower()
                    question = content

                    if bot_lang == 'cat':

                        query_word = 'consulta'
                        query_word_length = 8

                    elif bot_lang == 'eng':

                        query_word = 'ratios'
                        query_word_length = 6

                    if unidecode.unidecode(question)[0:query_word_length] == query_word:

                        try:

                            conn = None

                            conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                            cur = conn.cursor()

                            cur.execute("select local_reblogs_ratio, local_favs_ratio, fediverse_reblogs_ratio, fediverse_favs_ratio, reblogging_servers, faving_servers from ratios where username=(%s)", (username,))

                            row = cur.fetchone()

                            if row != None:

                                lr_ratio = row[0]
                                lf_ratio = row[1]
                                fr_ratio = row[2]
                                ff_ratio = row[3]
                                reb_servers = row[4]
                                fav_servers = row[5]


                                toot_text = "@"+username+ " " +"\n"
                                toot_text += "\n"
                                toot_text += toot_text1 +":\n"
                                toot_text += toot_text2 + ": " + str(lr_ratio) +"\n"
                                toot_text += toot_text3 + ": " + str(lf_ratio) +"\n"
                                toot_text += toot_text4 + ": " + str(fr_ratio) +"\n"
                                toot_text += toot_text5 + ": " + str(ff_ratio) +"\n"
                                toot_text += "\n"
                                toot_text += toot_text6 +":\n"
                                toot_text += toot_text7 + ": " + str(reb_servers) +" serv. \n"
                                toot_text += toot_text8 + ": " + str(fav_servers) +" serv. \n"
                                toot_text += "\n"

                            else:

                                toot_text = "@"+username + " " + toot_text9

                            cur.close()

                            toot_text = (toot_text[:400] + '... ') if len(toot_text) > 400 else toot_text

                            notif_type = "attended"
                            answered = True
                            queries += 1

                        except (Exception, psycopg2.DatabaseError) as error:

                            print(error)

                        finally:

                            if conn is not None:

                                conn.close()

                    else:

                        notif_type = "ignored"
                        ignored = True
                        answered = False

                    if answered == True:

                        if ignore == False:

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility )
                            print("Tooting")

                            ################################################################################################################################
                            # write the query to database

                            insert_query = 'INSERT INTO botreplies(datetime, id, query_user, notif_type, status_created_at, queries_total) VALUES(%s,%s,%s,%s,%s,%s)'

                            conn = None

                            try:

                                conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                                cur = conn.cursor()

                                cur.execute(insert_query, (now, status_id, username, notif_type, n_created_at, queries))

                                conn.commit()

                                cur.close()

                            except (Exception, psycopg2.DatabaseError) as error:

                                print(error)

                            finally:

                                if conn is not None:

                                    conn.close()

                            ################################################################################################################################

                            i += 1

                    if answered == False:

                        if ignored == True:

                            ################################################################################################################################
                            # write the ignored query to database

                            insert_query = 'INSERT INTO botreplies(datetime, id, query_user, notif_type, status_created_at, queries_total) VALUES(%s,%s,%s,%s,%s,%s)'

                            conn = None

                            try:

                                conn = psycopg2.connect(database = toots_db, user = toots_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                                cur = conn.cursor()

                                cur.execute(insert_query, (now, status_id, username, notif_type, n_created_at, queries))

                                conn.commit()

                                cur.close()

                            except (Exception, psycopg2.DatabaseError) as error:

                                print(error)

                            finally:

                                if conn is not None:

                                    conn.close()
                            i += 1

                except ValueError as v_error:

                    print(v_error)

                    cur.close()

            else:

                i += 1

        except (Exception, psycopg2.DatabaseError) as error:

            sys.exit(error)

        finally:

            if conn is not None:

                conn.close()

